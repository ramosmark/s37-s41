const express = require('Express')
const mongoose = require('mongoose')
// Allows our backend application to be available for use in our frontend application
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const courseRoutes = require('./routes/courseRoutes')

const app = express()
const port = 4000

// Middleware
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use('/users', userRoutes)
app.use('/courses', courseRoutes)

// Mongoose Connection
mongoose.connect(
	`mongodb+srv://ramosmark:admin123@zuitt-batch197.ixuhayw.mongodb.net/s37-s41?retryWrites=true&w=majority`,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
)

const db = mongoose.connection

db.on('error', () => console.error('Connection Error'))
db.once('open', () => console.log('Connected to MongoDB'))

app.listen(port, () => console.log(`API is now online at port: ${port}`))
