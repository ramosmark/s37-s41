const bcrypt = require('bcrypt')
const User = require('../models/User')
const Course = require('../models/Course')
const auth = require('../auth')

module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then((result) => {
		return result.length > 0
	})
}

module.exports.registerUser = (reqBody) => {
	const newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNumber: reqBody.mobileNumber,
		password: bcrypt.hashSync(reqBody.password, 10),
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({ email: reqBody.email }).then((result) => {
		if (result === null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(
				reqBody.password,
				result.password
			)

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result) }
			} else {
				return false
			}
		}
	})
}

module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then((result) => {
		if (result) {
			result.password = '****'
			return result
		} else {
			return false
		}
	})
}

module.exports.enroll = async (data) => {
	console.log(data.isAdmin)

	if (data.isAdmin) {
		return 'You cannot enroll because you are an admin'
	} else {
		const isUserUpdated = await User.findById(data.userId).then((user) => {
			user.enrollments.push({ courseId: data.courseId })

			return user.save().then((user, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			})
		})

		const isCourseUpdated = await Course.findById(data.courseId).then(
			(course) => {
				course.enrollees.push({ userId: data.userId })

				return course.save().then((course, error) => {
					if (error) {
						return false
					} else {
						return true
					}
				})
			}
		)

		if (isUserUpdated && isCourseUpdated) {
			return 'User successfully enrolled'
		} else {
			return 'Client cannot enroll, Error encountered'
		}
	}
}
