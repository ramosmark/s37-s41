const Course = require('../models/Course')
const auth = require('../auth')
const User = require('../models/User')

module.exports.addCourse = (req) => {
	const userData = auth.decode(req.headers.authorization)

	return User.findById(userData.id).then((result) => {
		if (result.isAdmin) {
			const newCourse = new Course({
				name: req.body.name,
				description: req.body.description,
				price: req.body.price,
			})

			return newCourse.save().then((course, error) => {
				if (error) {
					return false
				} else {
					return 'Course creation successful'
				}
			})
		} else {
			return 'You are not allowed to add a new course'
		}
	})
}

module.exports.getAllCourses = (req) => {
	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin) {
		return Course.find({}).then((result) => {
			return result
		})
	} else {
		return false
	}
}

module.exports.getAllActive = () => {
	return Course.find({ isActive: true }).then((result) => {
		return result
	})
}

module.exports.getCourse = (reqParams) => {
	// console.log(reqParams)
	return Course.findById(reqParams.courseId).then((result) => {
		return result
	})
}

module.exports.updateCourse = (req) => {
	const userData = auth.decode(req.headers.authorization)
	const courseId = req.params.courseId
	const reqBody = req.body

	if (userData.isAdmin) {
		const updateCourse = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
		}

		return Course.findByIdAndUpdate(courseId, updateCourse).then(
			(updatedCourse, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			}
		)
	} else {
		return 'You are not an admin'
	}
}

module.exports.archiveCourse = (req) => {
	const userData = auth.decode(req.headers.authorization)
	const courseId = req.params.courseId

	return User.findById(userData.id).then((result) => {
		if (userData.isAdmin) {
			return Course.findByIdAndUpdate(courseId, { isActive: false }).then(
				(updatedCourse, error) => {
					if (error) {
						return false
					} else {
						return 'Course has been archived'
					}
				}
			)
		} else {
			return 'You are not an admin'
		}
	})
}
