const jwt = require('jsonwebtoken')

const secret = 'CourseBookingAPI'

module.exports.createAccessToken = (user) => {
	// The data will be received from the registration form
	// When the user logs in, a token will be created with the user's info
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin,
	}

	// Generate a JSON Web Token using the jwt.sign() method
	// Generates the token using the form data and secret code with no additional options provided
	return jwt.sign(data, secret, {})
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	console.log(token)

	if (token) {
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {
			if (error) {
				return res.send({ auth: 'failed' })
			} else {
				next()
			}
		})
	} else {
		return res.send({ auth: 'failed' })
	}
}

module.exports.decode = (token) => {
	if (token) {
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {
			if (error) {
				return null
			} else {
				return jwt.decode(token, { completE: true }.payload)
			}
		})
	} else {
		return null
	}
}
