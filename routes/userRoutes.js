const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController')
const auth = require('../auth')

// Routes

// check if email is existinwg in db
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body).then((result) => {
		res.send(result)
	})
})

router.post('/register', (req, res) => {
	userController.registerUser(req.body).then((result) => {
		res.send(result)
	})
})

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then((result) => res.send(result))
})

router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	// console.log(userData)

	userController
		.getProfile({ id: userData.id })
		.then((result) => res.send(result))
})

router.post('/enroll', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	const data = {
		userId: req.body.userId,
		courseId: req.body.courseId,
		isAdmin: userData.isAdmin,
	}

	userController.enroll(data).then((result) => {
		res.send(result)
	})
})

module.exports = router
