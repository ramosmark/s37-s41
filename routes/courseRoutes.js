const express = require('express')
const router = express.Router()
const courseController = require('../controllers/courseController')
const auth = require('../auth')

router.post('/', auth.verify, (req, res) => {
	courseController.addCourse(req).then((result) => {
		res.send(result)
	})
})

router.get('/all', auth.verify, (req, res) => {
	courseController.getAllCourses(req).then((result) => {
		res.send(result)
	})
})

router.get('/', (req, res) => {
	courseController.getAllActive().then((result) => {
		res.send(result)
	})
})

router.get('/:courseId', (req, res) => {
	courseController.getCourse(req.params).then((result) => {
		res.send(result)
	})
})

router.put('/:courseId', auth.verify, (req, res) => {
	courseController.updateCourse(req).then((result) => {
		res.send(result)
	})
})

router.put('/:courseId/archive', auth.verify, (req, res) => {
	courseController.archiveCourse(req).then((result) => {
		res.send(result)
	})
})

module.exports = router
